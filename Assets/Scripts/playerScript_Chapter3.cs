using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerScript_Chapter3 : MonoBehaviour
{
    //personaje
    private BoxCollider2D collider;
	private Animator animator;
    private SpriteRenderer spritePersonaje;
    private AudioSource objetoConMusica;

    private int damage=1;
	public int maxHealth=1000;
	public int health;
    private GameObject objetoTrigger;
	private enemy_Attributes enemy;

	public healthBar_Script healthBar_Script;
	public AudioSource audioFondo;
	public AudioClip gameOverSound;
	public Canvas canvasGameOver;
	public Image imagenGameOver;
	public Sprite spriteGameOver;

	public AudioSource objetoConMusica2;

	public AudioClip andarEnRocas;
	public AudioClip ataqueCorto;
	public AudioClip ataqueCargado;



	private float startTime=0f;

    void Start(){
        //pillar lo del propio objeto
        collider=GetComponent<BoxCollider2D>();
		animator=GetComponent<Animator>();
		spritePersonaje=GetComponent<SpriteRenderer>();
		objetoConMusica=GetComponent<AudioSource>();

		//poner que la vida es la vida max
		health=maxHealth;

		//por si viene del gameover
		variablesGeneral.canMove=true;


    }

    void FixedUpdate()
    {
        float horitontalMove=Input.GetAxisRaw("Horizontal");
    	float verticalMove=Input.GetAxisRaw("Vertical");
    	
    	variablesGeneral.moveDelta=new Vector3(horitontalMove, verticalMove,0);
		RaycastHit2D hit;

		//reset animaciones
    	animator.SetBool("topMove",false);
    	animator.SetBool("BotMove",false);
    	animator.SetBool("horizMove",false);
        animator.SetBool("attack2", false);
		damage=1;
		startTime=0;

    	if(variablesGeneral.canMove){//apreta D
			if(variablesGeneral.moveDelta.x<0){
				animator.SetBool("horizMove",true);
				transform.localScale=Vector3.one;
				spritePersonaje.flipX = false;
				objetoConMusica.clip=andarEnRocas;
			}
			else if(variablesGeneral.moveDelta.x>0){//apreta A
				animator.SetBool("horizMove",true);
				spritePersonaje.flipX = true;//flip
				objetoConMusica.clip=andarEnRocas;
				//transform.localScale=new Vector3(-1,1,1);//si usas esto con la cam dentro, glichea. Mejor usar el flip del sprite
			}

			hit=Physics2D.BoxCast(
									transform.position,
									collider.size,
									0,new Vector2(variablesGeneral.moveDelta.x,0),
									Mathf.Abs(variablesGeneral.moveDelta.x*variablesGeneral.velocidad*Time.deltaTime),
									LayerMask.GetMask("Actor","blockWall")//con que layers interactua
								);
			if(hit.collider==null){//no hemos chocado
				//mover
				transform.Translate(variablesGeneral.moveDelta.x*variablesGeneral.velocidad*Time.deltaTime,0,0);
			}
			
		
			if(variablesGeneral.moveDelta.y<0){//apreta w
				animator.SetBool("BotMove",true);
				objetoConMusica.clip=andarEnRocas;
			}
			else if(variablesGeneral.moveDelta.y>0){//apreta s
	    		animator.SetBool("topMove",true);
	    		objetoConMusica.clip=andarEnRocas;
	    	}

			hit=Physics2D.BoxCast(
									transform.position,
									collider.size,
									0,new Vector2(0,variablesGeneral.moveDelta.y),
									Mathf.Abs(variablesGeneral.moveDelta.y*variablesGeneral.velocidad*Time.deltaTime),
									LayerMask.GetMask("Actor","blockWall")//busca objetos con estas layers
								);

			if(hit.collider==null){//no hemos chocado
				//mover
				transform.Translate(0,variablesGeneral.moveDelta.y*variablesGeneral.velocidad*Time.deltaTime,0);
			}


			if(Input.GetMouseButtonDown(0)){//ataque 2 (corto)
				animator.SetBool("attack2", true);
				objetoConMusica2.clip=ataqueCorto;
			}

			if(
				(animator.GetCurrentAnimatorStateInfo(0).IsName("ganmaAttack1") ||
				animator.GetCurrentAnimatorStateInfo(0).IsName("ganmaAttack2")) &&
				animator.GetCurrentAnimatorStateInfo(0).normalizedTime>0 &&
				objetoTrigger!=null){
				attack();
			}
			if(Input.GetMouseButtonDown (1)){startTime =Time.time;}

			if(Input.GetMouseButtonUp(1) && (Time.time-startTime)>0.2f){
				animator.SetTrigger("attackTrigger");
				objetoConMusica2.PlayOneShot(ataqueCargado);//si no pono este metodo glichea porque entra siempre aqui.
				//con PlayOneShot no hace falta play, por eso la condicion de parar/reproducir no está comprobando esta animación
				
			}//si es un click largo

            

			
			
            if(animator.GetCurrentAnimatorStateInfo(0).IsName("ganmaAttack1") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime>0){//esta haciendo la animacion
                damage=2;//daño doble
            }


            if(animator.GetCurrentAnimatorStateInfo(0).normalizedTime>1){damage=1;}//se acaba la animacion (cualquiera)




			if(health<=0){gameOver();}

			if(!objetoConMusica.isPlaying){
				objetoConMusica.Play();//reproduce
			}
			if(!animator.GetBool("topMove") && !animator.GetBool("BotMove") && !animator.GetBool("horizMove")){//ya no se mueve
				//still es que nada esta activado asi qu &&
				objetoConMusica.Stop();
			}


			if(animator.GetCurrentAnimatorStateInfo(0).IsName("ganmaAttack2")){
				if(animator.GetCurrentAnimatorStateInfo(0).normalizedTime>1){
					objetoConMusica2.Stop();
				}//si acaba, para
				else{
					objetoConMusica2.Play();
				}//sino, juega
			}
			

		}

    }


//----------------
    void OnTriggerEnter2D(Collider2D coll) {

		if(coll.gameObject.name=="enemigo_collider"){objetoTrigger=coll.gameObject;}
    }

	void OnTriggerExit2D(Collider2D coll) {objetoTrigger=null;}
//-------------------

	void attack(){
		//pillas el script
		enemy=objetoTrigger.GetComponent<enemy_Attributes>();
		//llamas al metodo
		enemy.reduceHealth(damage);
	}

	public void taken_Damage(int enemyDamage){
		
		if(variablesGeneral.canBeDamaged){
			if(health-enemyDamage==health-1){health-=enemyDamage;}
			healthBar_Script.SetHealth(health);
		}
	}

	public void gameOver(){
		audioFondo.clip=gameOverSound;
		audioFondo.Play();
		variablesGeneral.canMove=false;
		imagenGameOver.sprite=spriteGameOver;
		canvasGameOver.GetComponent<Canvas>().enabled=true;
	}
//-------------------
}
