using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class changeToFinalScene : MonoBehaviour
{
    public Animator animator;


    // Update is called once per frame
    void Update()
    {
        if(animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1){
            Invoke("LoadScene", 1f);
        }
    }

    public void LoadScene(){
        SceneManager.LoadScene("chapter3");
    }
}
