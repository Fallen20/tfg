using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scriptInstrucciones_chapter3 : MonoBehaviour
{
   public Canvas canvas;
   public Text text;

   void Start(){
      text.text="Move with WASD or directional arrows";
   	Invoke("instruction_attack",3f);
   }

   void instruction_attack(){
   	text.text="Attack with left click, hold and relase right click for a charged attack while moving horizontal.";
   	Invoke("instruction_objective",3f);
   }

   void instruction_objective(){
   	text.text="Objective: Find a way to escape without dying.";
   	Invoke("hideCanvas",3f);
   }

   void hideCanvas(){canvas.GetComponent<Canvas>().enabled=false;}
}
