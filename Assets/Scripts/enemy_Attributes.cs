using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy_Attributes : MonoBehaviour
{
    private int maxHealth=30;
    public int health;
    public TextMesh texto;
    public enemy_Move enemigo;

    private void Start() {
        health=maxHealth;
        texto.text=health+"/"+maxHealth;
    }
    void Update(){
        texto.text=health+"/"+maxHealth;
    }
    public void reduceHealth(int damage){
       health-=damage;
       if(health<=0){eliminar();}
    }

   public void eliminar(){
      int random= Random.Range(0,2);

      if(random==0){generate_Enemies_Start.created-=1;}
      if(random==1){generate_Enemies_Middle.created-=1;}
      if(random==2){generate_Enemies_End.created-=1;}

       
       enemigo.eliminarObjeto();
       Destroy(gameObject);
   }
}
