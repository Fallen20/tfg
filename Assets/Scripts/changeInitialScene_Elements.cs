using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class changeInitialScene_Elements : MonoBehaviour
{
    

    //canvas de la escena
    private Canvas canvasInicio;
    private Canvas canvasChapt;
    private Canvas canvasExtra;
    public Canvas canvasCredits;

    public Sprite[] imagenes;//a lo que cambia
    public Image imagenFondo;

    public static string chapterSelected="chapter 0";

    public Image flechaSeleccion;
    public Sprite flechaNegra;
    public Sprite flechaBlanca;

    public Text textoChapter0;
    public Text textoChapter1;
    public Text textoChapter2;
    public Text textoChapter3;

    public AudioSource objetoConMusica;

    // Start is called before the first frame update
    void Start(){
        chapterSelected="chapter 0";
        
        objetoConMusica.Play();

        //pillar los canvas
		canvasInicio=GameObject.FindWithTag("canvasInicio").GetComponent<Canvas>();
		canvasChapt=GameObject.FindWithTag("canvasChapt").GetComponent<Canvas>();
		canvasExtra=GameObject.FindWithTag("canvasExtra").GetComponent<Canvas>();

        //mostrar el de inicio
        canvasInicio.GetComponent<Canvas>().enabled = true;

        //ocultar los demas
        canvasChapt.GetComponent<Canvas>().enabled = false;
		canvasExtra.GetComponent<Canvas>().enabled = false;
		canvasCredits.GetComponent<Canvas>().enabled = false;

    }

    //------------------
    public void changeToChaptCanvas(){
        
        canvasChapt.GetComponent<Canvas>().enabled = true;

        canvasInicio.GetComponent<Canvas>().enabled = false;
        canvasExtra.GetComponent<Canvas>().enabled = false;
    }

    public void changeToextraCanvas(){
        canvasExtra.GetComponent<Canvas>().enabled = true;

        canvasInicio.GetComponent<Canvas>().enabled = false;
        canvasChapt.GetComponent<Canvas>().enabled = false;
    }


    //-----------------------
    public void changeImageChapter0(){
        chapterSelected="chapter 0";

        textoChapter0.GetComponent<Text>().color=Color.black;
        textoChapter1.GetComponent<Text>().color=Color.black;
        textoChapter2.GetComponent<Text>().color=Color.black;
        textoChapter3.GetComponent<Text>().color=Color.black;
        flechaSeleccion.sprite=flechaNegra;

        flechaSeleccion.GetComponent<RectTransform>().anchoredPosition = new Vector2(110, 165);//x y
        imagenFondo.GetComponent<Image>().sprite = imagenes[0];
    }

    public void changeImageChapter1(){
        chapterSelected="chapter 1";

        textoChapter0.GetComponent<Text>().color=Color.white;
        textoChapter1.GetComponent<Text>().color=Color.white;
        textoChapter2.GetComponent<Text>().color=Color.white;
        textoChapter3.GetComponent<Text>().color=Color.white;
        flechaSeleccion.sprite=flechaNegra;

        flechaSeleccion.GetComponent<RectTransform>().anchoredPosition = new Vector2(110, 55);//x y
        imagenFondo.GetComponent<Image>().sprite = imagenes[1];
    }

    public void changeImageChapter2(){
        chapterSelected="chapter 2";

        textoChapter0.GetComponent<Text>().color=Color.white;
        textoChapter1.GetComponent<Text>().color=Color.white;
        textoChapter2.GetComponent<Text>().color=Color.white;
        textoChapter3.GetComponent<Text>().color=Color.white;
        flechaSeleccion.sprite=flechaBlanca;

        flechaSeleccion.GetComponent<RectTransform>().anchoredPosition = new Vector2(110, -6);//x y
        imagenFondo.GetComponent<Image>().sprite = imagenes[2];
    }

    public void changeImageChapter3(){
        chapterSelected="chapter 3";

        textoChapter0.GetComponent<Text>().color=Color.white;
        textoChapter1.GetComponent<Text>().color=Color.white;
        textoChapter2.GetComponent<Text>().color=Color.white;
        textoChapter3.GetComponent<Text>().color=Color.white;
        flechaSeleccion.sprite=flechaBlanca;

        flechaSeleccion.GetComponent<RectTransform>().anchoredPosition = new Vector2(110, -72);//x y
        imagenFondo.GetComponent<Image>().sprite = imagenes[3];
    }
    //-----------------
    public void showCreditCanvas(){canvasCredits.GetComponent<Canvas>().enabled=true;}
    public void hideCreditCanvas(){canvasCredits.GetComponent<Canvas>().enabled=false;}

    public void backToMain_fromExtra(){
    	canvasExtra.GetComponent<Canvas>().enabled=false;//oocultas el que estas
    	canvasInicio.GetComponent<Canvas>().enabled=true;//sacas el que quieres ir
    }
    public void backToMain_fromChapterSelection(){
    	canvasChapt.GetComponent<Canvas>().enabled=false;//oocultas el que estas
    	canvasInicio.GetComponent<Canvas>().enabled=true;//sacas el que quieres ir
    }
}
