using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class resetChapter3 : MonoBehaviour
{

    public void restartGame(){
    	variablesGeneral.canMove=true;
    	generate_Enemies_Middle.created=0;
    	generate_Enemies_Start.created=0;
    	generate_Enemies_End.created=0;
    	
    	SceneManager.LoadScene("chapter3");
    }

    public void returnStart(){
        //restar las vars generales
        variablesGeneral.pillado1=0;
        variablesGeneral.pillado2=0;
        variablesGeneral.pillado3=0;
        variablesGeneral.pillado4=0;

        variablesGeneral.sleepDay2=false;
        variablesGeneral.sleepDay3=false;
        variablesGeneral.sleepDay4=false;
        variablesGeneral.sleepDay5=false;
        variablesGeneral.startDone=false;

        variablesGeneral.awamiKomaMission_WIP=0;
        variablesGeneral.awamiKomaMission_WIPBool=false;
        variablesGeneral.awamiKomaMission_Done=false;

        variablesGeneral.akaneMission_Done=false;

        variablesGeneral.talkedToRacoon=false;
        variablesGeneral.racoonMission_WIP=false;
        variablesGeneral.racoonMission_Done=false;

        variablesGeneral.ghoulMission_WIP=false;
        variablesGeneral.ghoulMission_Done=false;

        variablesGeneral.pumpkinFeatherPicked=false;
        variablesGeneral.kiyuFeatherPicked=false;
        variablesGeneral.whiskersFeatherPicked=false;

        variablesGeneral.santosMission_WIP=false;
        variablesGeneral.santosMission_Done=false;

        variablesGeneral.kiyuMission_Done=false;


        variablesGeneral.jumpscareShown=false;
        variablesGeneral.rennieAppeared=false;
        variablesGeneral.talkedToPumpkin=false;
        
        
        variablesGeneral.contador=0;
        variablesGeneral.contador2=0;
        variablesGeneral.num_random=0;
        variablesGeneral.contar=0;
        variablesGeneral.contar2=0;
        variablesGeneral.contar3=0;
        variablesGeneral.contar4=0;
        variablesGeneral.contarRennie=0;
        variablesGeneral.contarWhiskers=0;
        variablesGeneral.contarHemi=0;
        variablesGeneral.contarPumpkin=0;
        variablesGeneral.contarRacoon=0;
        variablesGeneral.contarSantos=0;
        variablesGeneral.contarKiyu=0;
        variablesGeneral.contarGhoul=0;
        variablesGeneral.correctBonePicked=false;


        variablesGeneral.canBeDamaged=true;

    	SceneManager.LoadScene("Initial_Scene");
    }
}
